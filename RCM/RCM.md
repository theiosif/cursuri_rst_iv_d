# Retele de Comunicatii Mobile

## 21.02.19 Curs 1

#### Notiuni Introductive


* __Generatia 1G__ a fost proiectata sa asigure numai un serviciu (cel vocal.)   
    * Exemplu: NMT: Nordic Mobile Telephonic; AMPS: ???
    * Functionare cu prelucrare analogica de semnal, in banda 450MHz sau 800-900Mhz.


* __Generatia 2G__ destinata pt servicii vocale __si date__.   
    * Banda 800-1000MHz
    * Exemplu: Sistemul GSM, D-AMPS (Digital AMPS)
    * 1991: Primul sistem GSM. Au fost cunoscute 1, 2, 2.5 faze.
    * In faza 2.5 GSM avea viteze de transmisie mai mari prin introducerea HSCSD si GPRS. --> devine posibila transmisa multimedia.


* __Generatia 3G__   
    * viteze de pana la 2Mb/s si in unele situatii pana la 8Mb/s
    * exemple: WCDMA si TD/CMDA
    * La nivel mondial 3G este recunoscut ca IMT2000  si in varianta europeana ca UMTS.
    * Date in utilizare in anii 2000-2001.


* __4G LTE (Long Term Evolution)__
    * Nu a reprezentat o inlocuire a UMTS ci o imbunatatire:
        * UMTS era proiectat pt fq de 25MHz
        * UMTS ne-flexibil, ne-scalabil
    * Scurtarea timpului intre transmisiuni a dus la cresterea impactului pe care multipropagarea il avea asupra semnalului receptionat.
    * LTE a propus ca in loc sa se imprastie spectrul semnalului in banda respectiva, el foloseste un alt tip de multiplexare (__OFDM__) --> avantaj: transmisiunea de date se va face prin subpurtatoare de banda ingusta (180kHz fiecare).
    * In locul unui singur flux rapid de date au aparut mai multe fluxuri de banda ingusta --> debitul de date e pastrat, dar efectul de multipropagare este puternic redus.
    * Pentru Prima data LTE nu mai are o arhitectura ierarhica ci una plana.
        * Avantaje:
          * viteza de stablilire a conexiunilor
          * handover realizat mult mai repede


* Diferente intre 4G si predecesori:
    * LTE a fost conceput ca o retea de date, tot traficul e bazat pe IP
    * VoLTE (voice over LTE)
    * Tehnologia MIMO (Multiple Input Multiple Output)
        * La inceput LTE suporta MIMO 2x2


* __LTE Advanced__: faza superioara a LTE - legatura spre 5G
    * viteze de 3 ori mai mari
    * agregarea purtatoarelor
    * MIMO cu mai multe antene 8x8
    * coordonare multipunct
    * statii releu
    * retea heterogena

* Agregarea purtatoarelor
    * schema de transmisie ce permite ca maxim 20 de canale din spectre diferite sa fie transmise in acelasi flux de date.


* Colaborarea MIMO:
    * permite echipaentelor mobile sa transmita si sa receptioneze semnale radio de la mai multe celule.
    * reduce interferenta
    * asigura performante mai bune la granita celulei.


* O statie releu LTE este o statei care receptioneaza un semnal de putere mica si il retransmite cu putere mai mare.

* Retea heterogena = sistem multinivel de celule, de dimensiune mica sau mare. Aceste celule se suprapun pt a mari largimea de banda.
    * Necesita atentie la proiectare deoarece introduce multe puncte de accesa

* SON (Self-Organizing Network) --> pasul spre 5G


* **Tehnologia 5G**
    * Conceputa sa asigure o societate complet mobila si interconectata
    * Cresterea capacitatii de 1000 de ori
    * Suport pt 10^12 conexiuni
    * Viteze de pana la 10Gbps
    * Latente <1ms


* **Diferente 4G vs 5G**
    * In timp ce 4G are ca scop utilizarea eficienta a benzii de frecventa, la 5G obiectivul este realizarea conectivitatii totale pt utilizatorii conectati la Internet.
    * In timp ce standardele LTE incorporeaza doar o aplicatie denumita MTC (Machine Type Communication), tehnologia 5G e proiectata de la inceput sa asigure comunicarea intre diferite echipamente/MTC-uri.
    * 5G nu au fost concepute sa fie monolitice, ci sunt o combinatie de 2G, 3G, LTE, LTE-A si WiFi.
    * Sunt concepute ca sa asigure o varietate mare de viteze a datelor: au linkuri highspeed si pt streaming HD, dar au si comunicatii de date de viteza mica (retele de senzori).
    * Retelele 5G se bazeaza pe **cognitive radio** (=BSS inteligente, care decide ce canale se aloca si in ce conditii).

#### Caracteristici generale ale sist. celulare.
* Zona de serviciu admite mai multe statii de baza.
* Sunt posibile localizari si inregistrari ale statiei mobile.
* posibilitatea transferului leg radio de la o BSS la alta (**handover** / **handoff**)
* Mai multe statii de baza pot utiliza aceleasi frecvente simultan.
* Extinderea sistemului se realizeaza de la un nr mic de celule dar cu diametru mare prin adaugarea unor celule de dimensiuni mici in zonelein care traficul e mare.
* Sist. celulare pot utiliza doua tipuri de antene:
      * omnidirectionale --> rezulta struct celulare regulate.
    * directionale --> structuri sectoriale

* Celule:  
  * Macrocelule:
    * Putere: 20-40W
    * Inaltime: 15-25m
    * Arie acoperire: 25-40km
    * Nr utilizatoiri simultani: >200 per sector & per frecventa
  * Macrocelule:
    * Putere: 2-5W
    * Inaltime:8-10m
    * Arie acoperire: 0.5-3km
    * Nr utilizatoiri simultani: 32-200
  * Macrocelule:
    * Putere: 5-4W
    * Inaltime: 5-15m
    * Arie acoperire: 1-10km
    * Nr utilizatoiri simultani: 32-64
  * Macrocelule:
    * Putere: 250mW
    * Arie acoperire: 250m
    * Nr utilizatoiri simultani: 32-64
  * Femptocelule:
    * Putere: 100mw
    * Arie: 50m
    * Nr utilizatori simultani: 8(rezidentiali), 16(intreprinderi)


* Avantaje femptocelule:  
    * creste capacitatea (scade distanta celula-utilizator)
    * putere mai redusa
    * sunt autonome (picocel sunt instalate de operator)
    * determina singure puterea la care pot sa opereze
    * transmit retelei automat cand sunt adaugate noi Femptocelule


* Dezavantaje femptocelule:
    * nemonitorizate (nu exista lista a canalelor libere) --> se poate pierde apelul

* Tipuri celule:
  * Celule echidimensionale (un sg strat)
  * Celule de dim diferite (un sg strat)      
  * Celule de dim diferite  (2/+ straturi)


* **Obs**: 2/+ straturi se mai nunmesc *celula umbrela*
* **Obs**: Reteaua cu cel de baza in varfurile neadiacente ale hexagonului (tip B, C) se obtine printr-o translatie a retelei cu BSS in statiile de baza plasate in centrul hexagonului (tip A)
* **Obs**: Utiliz antenelor sectoriale determina impartirea grupului de canale in 3/6 sectoare

---

## 27.02 Curs 2

* In sistemele mobile o problema esentiala o reprezinta fenomelele aparute ca urmare a propagarii in mediul liber. Semnalul receptionat, fie de un mobil, fie de o statie de baza, cuprinde o componenta datorata propagarii directe intre emitator si receptor, dar si o suma de componente provenite din reflexii si refractii datorate topologiei mediului (teren, cladiri, vegetatie - tot ce poate fi situat intre RX si TX).

* O prima clasificare a distorsiunilor din mediul radio:
  * Distorsiuni de frecventa (efectul Doppler) - datorate miscarii mobilelor
  * Distorsiuni de amplitudine (fading/ atenuare Rayleigh)
  * Distorsiuni de faza - datorate propagarii multicale


* O alta clasificare a factorilor care afecteaza calitatea semnalului transmis:
  1. Distanta emitator-receptor:
    * Semnalul este atenuat proportional cu inversul patratului dist(TX, RX), cu relatia:   
    * ![1](C1_1.png)   
        * lambda=c/f   
    --> **din aceasta cauza se fol fq inalte in sistemele de comunicatii mobile**
  2. Reflexia apa/sol:
      * Semnalul RX are 2 componente: una directa si una obtinuta prin relfexie (componentele nu sunt in faza --> distorsiuni)   
      ![1](C1_2.png)    
      ---> Efectul de distorsiune e mai puternic daca luam in calcul si reflexiile
  3. Absorbtiile atmosferice (oxigen, apa din atmosfera)
  4. Efectele de umbrire (obstacole intre BSS si RX)
  5. Difractii multiple - datorate denivelarilor de teren

* Toate efectele enumerate produc asa-numita atenuare(fading) pe arie larga si reprezinta o mediere a semnalului efectuata pe o lungime de cateva zeci de lungimi de unda.
* Exista si atenuare(fading) local - pe cateva lungimi de unda.
* Cauze fading local:
  1. Cai multiple de propagare
  2. Viteza mobilului - fading datorat efectului Doppler
    Efect Doppler: deviatie a fq a semnalului RX fata de semnalui TX   
    ![1](C1_3.png)    
  3. Inexistenta unei cai directe de propagare intre mobil si statia de baza (fading Rayleigh)


* Ca o concluzie, atenuarea canalului radio are 3 componente (toate exprimate in decibeli):
  1. A1 = atenuarea datorata dist(TX, RX)
  2. A2 = atenuarea datorata efectului de mascare (!E cale directa TX, RX)
  3. A3 = atenuarea datorata propagarii pe cai multiple

  ![1](C1_3.png)


* In afara de aceste atenuari mai exista si interferente pe canalul radio (intre semnalul util si alte semnale radio):
  * Interferente independente de sistemul mobil (datorate zgomotelor parazite urbane/industriale)
  * Interferente datorita reutilizarii frecventelor in sistemul mobil   
    Interferenta intre celule care utilizeaza aceleasi canale de fq: **Interferenta co-channel**
  * Interferente de canal adiacent: Utilizatori diferiti, aflati in aceeasi arie geografica, folosesc canale adiacente in spectru.

* Un semnal radio-mobil r(t) este caracterizat de 2 componente:
  * m(t) - componenta locala - fading pe termen lung
  * r0(t) - fading pe termen scurt/Rayleigh

  Componenta medie locala m(t) se poate obtine astfel:
  ![5](C1_5.png)

### **Interferenta Co-Channel**
* Notatii:
  * R = raza celulei
  * D = distanta intre celule interferente co-channel
  * Q = factorul de reducere al interferentei co-channel


* Se doreste determinarea Q a.i sa avem SNR de 18dB intre semnalul purtator si Interferenta:   
    * **C/(N0+I) = 63.1**
        * C - nivel purtator
        * I - nivel Interferenta
        * N0 - nivel zgomot
        * M - nr celule interferente

* Pp ca sgn e atenuat prop cu R^-4 --> 40dB/decada si aprox N0 << I
* Purtatorul = R^-4    
  ![1](C2.png)
* **Obs**: Q independent de puterea transmisa, dar dependent de nr de celule interferente (6).
* 2 cazuri de proiectare in cazurile cele mai defavorabile:   
    a. cu antene omnidirectionale   
    b. cu antene directionale

#### Cu antene omnidirectionale

* Distantele intre care facem celulele sunt: D-R, D, D+R, (D-R)/2, (D+R)/2
  * Semnalul va fi proportional cu:  
    * (vezi poza de deasupra)

![2](C3_3.png)
* Ar putea fi folosit un nr mai mare de celule cu aceeasi frecventa, dar va scade numarul de canale utilizate intr-o celula, si eficienta reutilizarii. --> Se folosesc antenele directionale.

#### Cu antene directionale
* Exista antene directionale cu 120 si 60    
* Dezavantajul antenelor directionale:
  1. Creste numarul de antene
  2. Creste numarul de handover-uri

* Pt antelele cu sector de 120, in cazul cel mai defavorabil, raportul S/I:
![4](C3_4.png)
![4](C3_5.png)    
* Pt antenele cu sector de 60, raportul S/I devine:
    * S/I=(Q+0,7)^4/I=29dB (vezi sus)


#### Mecanismul de transfer al apelurilor (Handoff/Handover)
* Se face automat, fara interventia abonatului.
* Apar 2 probleme:
    * Realizarea cu succes a handover-urilor necesare
    * Reducerea numarului de handover-uri inutile in sistem (apar la situatii inainte si inapoi la granita celulei)

* La granita celulei: puterea alfa. Decizie de handover la (alfa-delta).
    * Daca **delta e prea mare**, atunci mobilul are timp sa se intoarca in interiorul celulei sau sa se opreasca. Atunci multe handoveruri nu se vor realiza pentru ca decizia a fost luata prea devreme.
    * **Delta prea mic**, atunci nu va fi timp sa se realizeze un handover si multe apeluri se vor pierde.


* Exista si situatii in care handoverurile sunt necesare dar nu se pot efectua. E.g.: Cand puterea scade sub un anumit nivel, dar fie mobilul nu se afla la granita celulei, fie nu exista canale libere. In ambele cazuri se mentine apelul pe acelasi canal cat de mult posibil si se incearca o realocare.


* La cresterea traficului se va face o **divizare a celulelor**.
* Divizarea celulelor:
    * R <- R/2
    * A <- A/4
    * Not. PR = puterea receptionala, PT1=puterea transmisa in situatia initiala
        * PR = alfa \* PT1 * R^-gama
        * PR = alfa \* PT2 * (R/2)^-gama
        * PT2 = PT1 * (1/2)^-gama
        * --> puterea transmisa dupa divizare poate fi cu 12dB mai mica decat in primul caz.
        * --> Q la fel: Q=R/D


#### Accesul aleatoriu in sistemele mobile
* Un sistem poate sa aiba acces la MT in orice moment, cu unele restrictii.
* **Dezavantaj**: Pot exista coliziuni daca 2/+ transmit in acelasi timp. Datorita coliziunii pachetele de date se pierd si trebuie retransmise. Au fost propuse diferite metode de evitare a coliziunilor


* Algoritmul **p-ALOHA**
  * 4 moduri de lucru:
    * *modul transmisiune*: un abonat nu e interesat de activitatea celorlalti utilizatori si transmite cand doreste
    * *modul receptie*: dupa ce a transmis, abonatul trece pe receptie si asteapta mesaje de ACK. Daca TX s-a suprapus partial/total cu a altui abonat inseamna ca a aparut o coliziune si va primi o confirmare negativa (NACK)
    * *modul de reTX*: daca s-a RX un NACK, mesajul este retransmis. Daca reTX se face imediat, apare o coliziune. --> reTX se va face dupa un interval aleator.
    * *modul de expirare al timpului de confirmare (Timeout)*: Daca mesajul de (N)ACK nu soseste intr-un timp specificat, transmiterea pachetului se va relua.
  * Determinarea eficientei:
    * Vom considera ca fiecare pachet are *b* biti, rata de TX a pachetelor acceptate este *lambda*, rata pachetelor rejectate este *lambda_r* si rata de transmisie totala este *lambda_t*. Evident: *lambda_t = lambda + lambda_r*
    * Debitul de transmisie al informatiei: *ro' \* =b* lambda biti/s
    * traficul total: *G' = b \* lambda_t biti/s *
  * Un pachet intra in coliziune cu un altul daca al doilea pachet incepe sa fie transmis intr-un interval mai scurt decat *tau* inaintea primului bit al pachetului initial.


---

## 28.02 Curs 3

* _tau = b / R_
* Debitul = ro = lambda * traficul
* Traficul = G = lambda_t * tau

![1](C3_01.png)
* Probabilitatea ca sa se transmita K pachete:
  3.01xxx
  * Aceasta p a fost calculata ca un proces de tip poisson deoarece utilizatorii transmit independent si sporadic pachete de date.
![2](C3_02.png)
* Pt teta = 2*tau si k=0 (niciun pachet transmis), vom avea probabilitatea p0=
  3.02xxx
* P0 e dat si de relatia lambda/labda_t=ro/g=G*e^(-2G)
* (fig3.1xxx): in cel mai favorabil caz se foloseste doar 18% din capacitatea canalului


* Algoritmul **s-ALOHA**
  * pastreaza aproape toate caract lui p-aloha dar se impune ca transmisa sa se faca numai in niste intervale temporale, create prin transmiterea unor impulsuri de sincronizare
  * se considera ca durata ferestrelor este tau si ca emisia trebuie sa inceapa imediat dupa ce se primeste un impuls de sincronizare
  * ![3](C3_03.png)
  * coliziunile apar doar cand 2 useri transmit simultan


* Algoritmul **R-ALOHA** // R-rezervare
    * Pastreaza aproape toate caracteristicele lui P-Aloha dar o statie emite doar dupa ce face rezervare. In afara de canalele de trafic exista si canale mai scurte dedicate rezervarilor.
    * Intr-un interval de rezervare, terminalul comunica resursei ca are de transmis date, iar controllerul ii aloca un anumit numar de intervale.
    * Ca urmare, exista 2 moduri de lucru: rezervat si nerezervat.
        * mod nerezervat:
            * timp impartit in intervale scurte
            * exista la pornire, si atunci cand niciun utilizator nu transmite
        * modul rezervat:
          * incepe numai dupa ce a fost acceptata o cerere de rezervare
          * presupunem ca in modul rezervat, t_r este impartit in M intervale temporale
              * dintre acestea, M-1 sunt destinate traficului, iar 1 este divizat in intervale de rezervare


![4](C3_04.png)

**Tehnici de interogare**
* Cand numarul de utilizatori creste, accesul aleator nu este deloc recomandat
* Interogarea directa poate duce la rezulatate acceptabile daca cererile de comunicare sunt destul de numeroase. Daca nu, trebuie sa se gaseasca o solutie care sa elimine rapid care statii nu sunt active.
* Arborele de cautare binar:
  * Se imparte nr de utilizatori in 2, si se retine pt comunicatie o unitate, si procesul se reia pana ramane un sg solicitat.
  * Eg: o resursa lucreaza cu 8 statii (000-111):
    * Controllerul le cere sa transmita MSB-ul din nr lor de ordine. Daca a primit si biti de 1, si biti de zero, dupa un criteriu oarecare (nivelul semnalului), decide sa le serveasca pe cele cu bitul 1. Statiile care au bitul 0, trec in asteptare.
    * Cere statiilor sa trimita  bitul al 2lea, si selecteaza iar pe cele cu b=1
    * Se continua operatia pana ramane o singura statie care va emite. Cand statia a terminat de transmis, procedura se va relua pana cand sunt deservite toate solicitarile.

* Care metoda e mai buna?
  * Sa presupunem ca avem M statii, iar tau = timp interogare pt o statie. La arborele binar, o trecere necesita log2M interogari.
  * Daca avem M' solicitari, atunci T_ab = (M' \* log2M) \* tau
  * Arborele binar e mai eficient daca: M'log2M <= M. Adica M'\_max = M/log2M

* Acces aleatoriu cu ascultarea purtatoarei (CSMA):
  * Un sistem gata sa transmita ascuclta mediul de transmisiune. Daca il gaseste liber, va incepe sa emita.
  * Daca mediul e ocupat, se amana transmisiunea
  * Exista mai multe variante de acces aleatoriu, in fct de strategia urmata cand se detecteaza mediul ocupat, modul in care se detecteaza coliziunile si strategia urmata dupa detectarea coliziunilor (vezi CD):
    * CSMA nonpersistent
    * CSMA persistent
      * Se micsoreaza probab de coliziune, dar pot exista pierderi de timp, pt ca la eliberarea mediului de transmisiune, desi 1/+ sisteme pot fi gata sa transmita
    * slotted.


* CSMA-CD
  * urmatoarele moduri de lucru:
    * _Detectia purtatoarei_: un utilizator nu trebuie sa transmita daca este prezenta o purtatoare, de asemenea nu trebuie sa transmita dupa disparitia unei purtatoare pe durata unui pachet.
    * _Transmisia_: utilizatorul transmite pana cand termina/ detecteaza o coliziune
    * _Renuntarea_: (Blank), el transmite si un semnal de bruiaj pentru a anunta atat ceilalti utilizatori ca a aparut acea coliziune.
    * _Retransmisia_: Utilizatorul asteapta un interval aleator inainte de a realiza o retransmitere.
  * Pachetele au o structura de date fixa, singurul element variabil este lungimea campului de date.
  * ![1](C4_01.png)
    * preambulul permite sincronizarea
    * Adr_dst:
      * 111.111 <-> broadcast
      * MSB=1 <-> multicast
      * MSB=0 <-> unicast
    * intervalul intre pachete este de 9,16 us
    * codare Manchester


#### Schimbarea Frecventelor (Frequency Hopping)
* = schimbari la intervale regulate ale frecventei
* 2 tipuri de salt de fq:
    * SFH = slow frequency hopping
    * FFH = fast frequency hopping (fq TX se schimba de mai multe ori pe durata unui simbol)
* In comunicatiile mobile se foloseste SFH
    * Diversitatea frecventei
      * Fadingul produce erori grupate --> se pot evita pe un segment vocal (toate grupate doar pe un sg segment vocal), bitii fiecarui segment vocal se impart in 8 pachete, si folosind SFH fiecare din cele 8 se TX pe o alta frecventa.
      * Cum fadingul este dependent de frecventa, inseamna ca pachetele succesive nu vor fi afectate la fel de fading --> imbunatatim transmisia
      * (blank): in sistemele mobile se prefera ca puterea totala a semnalelor de interferenta sa fie distribuita cu o pondere mai mica pe cat mai multe apeluri, decat cu o pondere mare pe cateva apeluri.
      * Prin SFH, se asigura aprox acelasi nivel de interferenta pt orice apel, pt ca pe unele fq interferenta e mai mare, iar pe altele este mai mica.

* Capacitatea sistemelor celulare:
    * Notatii:
        * B_t = banda totala
        * M = B_t / B_c = nr total de canalelor
        * (C/I)\_s = raport minim necesar purtator/interferenta/canal sau interval temporal
    * In sistemele FDMA/TDMA avem m=M/rad(2/3)(C/I)\_s
    * Raportul C/I receptionat, se poate exprima in functie de:
        * E_B: Energia per bit
        * I_0: Puterea semnalului interferent/Hz
        * R_b : nr biti per secunda
        * B_c: largimea de banda a canalului radio (Hz)
          * ** C/I =E_B/I*
        * In FDMA sau TDMA, apelurilor le sunt asignate canale de fq sau interval temporale. Astfel, RBBc si Eb/I0>1 intotdeauna, si ca urmare C/I>1
        * (Blanks)

**Capacitatea sistemelor celulare de tip CDMA**
  * Q_A = Q Adiacent = 2
    * _Adiacent == acelasi canal radio poate fi refolosit in celulele vecine_
  * Q_S = Self Q = 1
    * Datorat faptului ca aceleasi fq de post sunt folosite in aceeasi celulare


---
## 08.03 Curs 4

* Capacitatea radio a sistemului CDMA
  * ![1](C4_02.png)
* In cazul in care se utilizeaza scheme de control al puterii, capacitatea radio a sistemlui creste (se reduce interferenta intre celule adiacente): CI/S devine:
  * ![1](C4_03.png)
  * Pt valorile specificate anterior rezulta:
    * (C/I)s = 0.032 --> M = 30.25
    * C/I)s = 0.01792 --> M = 54.8

* Datorita faptului ca toate canalele de trafic folosesc acelasi canal radio, apare urmatoarea problema: Un semnal puternic receptionat de la un mobil aflat in apropierea statiei de baza va masca un semnal mai slab de la un vehicul care se afla la distanta mare fata de statia de baza.
  * se va utiliza o adjustare a puterii  pe baza distantei fata de celula considerata:
  * ![1](C4_04.png)
  * __Obs__: C/I >= (C/I)s
  * Introducant valoarea C/I)s in ecuatia anterioare, se obtine:
    * M = 30.25, m=22.74 pt C/I)s=-15dB
    * M=54.5, m=4.2 pt C/I)s=-17dB

#### Avantajele sistemelor CDMA
  * __Va creste capacitatea canalului__: Se stie ca ciclul activitatii vocale este de 35% (restul e dedicat ascultarii/ pauzelor din convorbire). La CDMA, acest timp de 65% care ramane este folosit de ceilalti utilizatori ai canalului radio care pot comunica intre ei cu minim de interferenta
  * __Nu sunt folosite egalizoare__: (_blank :(_ ) In CDMA, pentru reducerea interferentei de simboluri se foloseste un corelator la receptie care este si mai simplu de instalat, si mai putin costisitor decat egalizorul.
  * __Realizarea software a handover-ului__: se trece de la o secventa de cod la alta secventa de cod.
  * __Nu exista timp de garda__: In TDMA e necesar sa se foloseasca timp de garda intre sloturi (acesti biti nu pot fi folositi pentru altceva)
  * __Scaderea fading-ului__
  * __Nu e necesar un mecanism de control/alocare a frecventelor__

### __Sistemul GSM__
* Global System for Mobile communications
* Unitatile functionale principale ale sistemlui GSM
![1](GSM.png)


* Acronime:
  * MS = mobile station
  * BSS = base station System
  * MSC = mobile switching center (centru de comutatie pt reteaua mobila)
  * PSTN = public switch telephone network
  * ISDN = Integrated Services Digital Network
    * TUP(telephone user part) / ISUP (ISDN user part) - interfete
  * VLR = baza de date care contine pozitiile curente ale statiilor mobilelor
  * HLR = baza de date cu toate informatiile de baza ale statiilor mobilelor
  * AUC = Auth Center
  * EIR = registru pt identitatea echipamentelor
  * OMC = operation & maintenance center



* __MSC: Centrul de comutatie__ (mobile switching center)
  * Asigura prelucrarea traficului de la acei abonati care se afla la un moment dat in zona lui de serviciu.
  * Fiind o centrala telefonica, MSC-ul are si caracteristici pe care le intalnim la centrale telefonice clasice, dar si specice unei retele mobile.
  * __Functii__:
    * Gestioneaza datele pentru taxare si le transmite unui centru de taxare
    * Masoara si inregistreaza traficul pentru a evalua utilizarea retelei si pentru decizii de rutare a traficului
    * Rutarea apelului in retea
    * Alocarea linkurilor de iesire
  * __Functii specifice unui sistem de comunicatii mobile__:
    * Inregistrarea vehiculelor din zona lui de acoperire
    * Activarea procedurilor de control al accesului la canalele radio: se valideaza identitatea statiei
    * Controlul informatiei primite de la statiile de baza privind pozitiile curente ale mobilului
    * Autentificarea mobilelor si inregistrarea lor in VLR
    * Realizarea procesului de handover
    * Interogarea registrului HLR
    * Schimba informatii de semnalizare prin intermediul interfetelor specializate si mentine si controlul acestor interfete.
    * Mentine sincronizarea cu reteaua mobila terestra publica (PLMN - public land mobile network), dar si cu statiile de baza.


* In HLR sunt memorate toate informatiile de baza ale unui mobil. Cel mai important este numarul __IMSI__ (international mobile Subscriber Identity).   
__Obs__: IMSI e fix numaru de telefon basically
* Structura __IMSI__:
  * MCC: Mobile Country Code
  * MNC: Mobile Network Code
  * MSIN: MS Identity Number


* Identificare temporara:
  * __TMSI__: Temorary Mobile Subscriber Identity --> folosit numai in comunicarea dintre MSC si valorile (din considerente de securitate)
  * __MSRN__: Mobile Subscriber Roaming Number --> alocat statiei mobile la cererea HLR-ului cand statia mobila trece intr-o alta arie de numerotare.

* Schema bloc MSC:
    * ![msc](MSC.png)
    * Acronime:
        * BDP =  Billing Data Processor
        * AMA = Automatic Message Accounting
            * Colecteaza datele de taxare de la BDP si le prelucreaza/memoreaza si transmite catre OMC
        * MCP = Matrix Control Processor
        * GS = Group Switch
        * LCM = Land Call Management
      * Gestiunea si inregistrarea linkurilor libere catre PSTN, alte MSC-uri si catre BSS-uri.
      * Masuratori de performante de trafic pe linkurile catre PSTN, MSC, BSS
      * Comenzi de paging catre statia mobila
      * Procesarea apelurilor in PLMN
      * Monitorizarea alarmelor si notificarea OMC-ului
    * MM = Mobility Management
        * (blank)
        * teste si optimizari de circuite
        * colecteaza diferite statistici
    * SM = Service Management
    * SS7 = semnalizare pe canal comunica
    * IF = interfetele de comunicare pe protocolul X.25


* __BSS: Sistemul statiilor de baza__
  * Alcatuit din:
    * Controller-ul statiei de baza (BSC)
    * Base Transciever Station (BTS)
  * La un controller se pot conecta 1/+ BTS, dupa urmatoarele configuratii:
    * (xxx4.07)
  * (blank) antene omnidirectionale: Avem un BTS si un BSC in acelasi loc fizic
  * Cfg in stea/lant
  * Cfg urbana, cu 3 BTS-uri in acelasi loc fizic


  * ![5](C5_01.png)
    * TRX = Transciever
    * BCF = Base Control Function
    * BSC = Base Station Controller
    * __a__: un BCF si un TRX pe un canal fizic
    * __b__: un BCF si N TRX pe un canal fizic
    * __c__: un BCF si N TRX pe N canale fizice

* __BSC: Base Station Controller__
  * ![bsc](BSC.png)
  * Matricea de comutatie ofera pana la 64  de cai de acces, fiecare fiind capabila sa comute intervalele temporale cu debitul de 64kbps, canale care apartin fluxurilor de date cu viteza de 2Mbps.
  * Un BSC poate sa controleze pana la 60 de purtatoare, si cum o purtatoare are 8 canale --> pana la 480 de canale radio.
  * Controllerul de interfata:
    * controleaza legatura de 2Mbps catre subsistemul retea, si anume realizeaza controlul pe baza SS7
    * Asigura comutarea la matricea de comutatie
  * Controllerul de terminal:
    * Asigura controlul purtatoarei radio (canalele sunt si de trafic, si de semnalizare).
    * Asigura si controlul cu echipamentele de transmisie
    * Asigura stabilirea a 4 legaturi de semnalizare catre statiile de baza cu ajutorul protocolului LAPD.
    * Prelucreaza masuratorile de pe canalele de trafic de pe purtatoarea asociata
  * Procesorul central:
    * Executa functiile centralizate ale sistemului
    * Asigura gestiunea legaturii catre OMC, link pe care opereaza protocolul X.25


  * Functii BSC:
    * gestioneaza impreuna cu OMC canalele radio
    * gestioneaza canalele de trafic
    * [!] la solicitarea unui abonat, BSC-ul este cel care alege canalul si celula optima (intr-o zona acoperita de mai multe celule). Canalul se alege pe baza unor mesaje care cuprind descrierea canalului.
    * gestionarea canalelor radio de difuzare a informatiei si a canalelor de control comun
    * indica resursele radio: BSC-ul primeste de la BTS periodic care sunt canalele nefolosite si le tine intr-o baza de date

---
## 13.03 Curs 5
### __BTS (Base Transciever Station)__

__Rol__: Asigura legatura radio cu statia mobila si transmite semnalele catre si de la BSC.

* Unitati Principale:
    * Unitatile de ceasuri
    * Matricea de salt de fq
    * Echipament de test de radiofq
    * Unitatea de purtatoare
    * Unitatea de cuplaj


* Tot din BTS se considera ca face parte unitatea pentru adaptare de citeza si transcodare (__TRAU__: Transcoder Rate Adapter Unit)

![BSC2](BSC2.png)
* Exista 3 cazuri posibile
    * ![BSC3](BSC3.jpg)
    * ![BSC3](BSC32.png)

#### __Unitati Principale__

* Unitatea de ceasuri
    * Contine fq purtatoarei de ref 13MHz si pot genera tactele si semnalele de sincronizare necesare BTS.
    * Daca in acelasi aplasament se instaleaza mai multe BTS-uri, un singur BTS va fi echipat cu unit. de ceasuri.

* Echipamentul de teste de radiofq:
    * cuplat la antena de E/R
    * are un sinstetizor de fq si permite sa se inchida in banda orice canal de trafic al unei prutatoare, fara ca prin aceasta sa se perturbe traficul pe celelalte canale (folosit pt a alocaliza defectele de pe lantul de transmisie, pt testarea si remedierea defectiunii.
    * [Blank poza]


* Unitatea de prelucrare in banda de baza
    * Rol: prelucreaza esantionale receptionate sau emise de catre unitatea de purtatoare prin intermediul aceleli matrici denumita _matrice de salt de frecventa_.
    * Efectueaza masuratori de calitate asupra semnalului receptionat
    * prelucreaza canale radio de semnalizare
    * gestioneaza protocoalele de transmise catre statia mobila si catre BSC.
    * Obs: La receptie, esationale reprezinta datele de intrare pt algoritmii de _egalizare_.


* Unitatea de purtatoare
    * transpune semnalul din banda de baza in banda UMTS.
    * GSM functioneaza pe uplink in banda 890-915MHz. Pe downlink: 935-960Mhz


* Unitatea de cuplaj
    * contine cuplorii multipli pt receptie


#### __Functii BTS__
* emisia si receptia pe canelele radio alocate
* realizarea avansului in timp - MS trebuie sa isi modifice continuu mom de transmisie pt a putea compensa variatia distantei dintre MS si BTS.
* Masuratori de propagare si calitate pt uplink + transmiterea rezultatelor periodic BSC-ului.
* Receptioneaza rez masuratorilor de propagare efectuate de MS.


### __Statia Mobila__ (MS)
![MS](MS.png)
* Formata din echipamente radio si interfata necesara unui abonat pt a avea acces la serviciile oferite de reteaua terestra publica (__PLMN__)
* Comunicatia MS<->BS se realizeaza prin interfata de baza __Um__.
* Prin __Um__ se transmit canalele de trafic si canalele de control si semnalizare.
* Pt a se realiza o distinctie intre identitatea abonatuluoi si achipamentele mobile se foloseste cartela SIM (Subscriber  Identifier Module).
* SIM:
    * IMSI
    * TMSI


#### Identificarea ariilor de localizare a celuleor si a statiilor de baza
* O arie de localizare (__LAI__ - Local Area Indentity) are campurile:
    * MCC - Mobile Country Code
    * MNC - Mobile Network Code
    * LAC - Local Area Code


* Identitatea celuleor - CGI - Cell Global INterface:
    * MCC
    * LAI
    * MNC
    * CI (Cell Identity)

* Identificarea statiei de baza: BSIC (Base State Identity Code): MCC + BCC
* Dialogul cu reteaua: cand intra intr-o noua arie de acoperire.


* __Statia mobila - PLMN__
    1. Datele statiei sunt actualizate si este permisa deplasarea
        * Pot incepe procedurile de stabilire a apelului.
    2. Datele statie sunt acutalizate, dar nu este permisa deplasarea
        * Statia mobila trebuie sa verifice datele ei si sa ceara inscrierea corecta a datelor pt permiterea deplasarii
    3. Cand se constata o eroare in actualizarea datelor statiilor:
        * statia mobila, dupa un timp, va incepe o noua actualizare si daca acum se constata eroare atunci va incepe procedura de actualizare completa a datelor statiilor.
    4. Identificare insuficienta
        * Reteaua nu poate identifica statia mobila si statia mobila va initia o noua actualizare, utilizand IMSI.
    5. Statia mobila nu este inregistrata in HLR
        * legatura cu aceasta statie se va obtine dupa ce se face inregistrarea in HLR si dupa ce se repeta actualizarea datelor.
    6. Abonat ilegal - nu e permis accesul in sisteme


#### Functiile unei MS


---
## 14.03 Curs 6
Principalele date cu caracter personal din VLR sunt:
  * numarul (??) - caracter temproar
  * datele de autentificare si cifrare
  * numarul MSRN
  * adresa MSC-ului
  * parametri pentru servicii suplimentare:
    * codul pt realizarea de apeluri prestabilite
    * intervalul de timp dintre momentul initierii unui apel si momentul cand se realizeaza practic legatura
    * codul pt blocarea unor apeluri catre grupuri inchise de utilizatori
    * indicatorul de astptare a uni mesaj (in cazul mesajelor scurte)


  * VLR interactioneaza cu MSC pt activarea urmatoarelor proceduri:
    * inregistrarea pozitiei mobilului
    * atribuirea/suspendarea numarului IMSI
    * autentificare
    * cautarea unei statii mobile


  * OMC (Operation and Maintenance Center):
    * raporteaza si inregistreaza toate alarmele generate de diferite blocuri
    * face teste si diagnoza asupra starii functionale a retelei
      * cand este cazul, se pot rejecta apelurile de catre BSS
    * colecatarea datelor de trafic de la celelalte blocuri de trafic din GSM


  * Centru de taxare:
    * centralizeaza datele pt taxare de la diverse unitati ale sistemului
    * in general se recomanda ca fiecare MSC implicat intr-o convorbire sa inregistreze separat datele pentru taxare.
    * datele de taxare sunt inregistrate in MSC-ul chematorului


  * Centrul de intretinere a retelei
    * OMC-ul opereaza doar la nivel regional, pe cand centrul de intretinere asigura operarea si intretinerea la nivelul intregii retele
    * Functii:
      * Coordonarea retelei la nivel national
      * Supravegherea conectarii dintre PSTN (reteaua fixa) si reteaua mobila
      * Urmareste aspecte legate de trafic la nivel national
      * Monitorizeaza reteaua pt alarme de nivel superior (noduri scoase din functiune, supraincarcari)
      * Urmareste semnalizarile si rutele pt traficul dintre noduri
      * In unele cazuri, poate interveni in BSS pt dirijarea traficului.
      * Daca sunt probleme cu OMC-ul, poate prelua controlul regional

  * Interfete de GSM:
    * Interfata ABIS e in interiorul sau.

  ![2](C6_1.jpeg)

  * (balnk)
  * Gruparea canalelor se paote face pe bit sau pe combinatii de cod
    * Gruparea pe bit: fiecarui canal i se atribuie un interval temporal corespunzator unui bit (TDMA pe biti gen..)
    * Gruparea pe combinatii de cod: Fiecarui canal i se atribuie un interval de timp mai mare, corespunzator unui numar oarecare predeterminat de biti.

    ![2](C6_2.jpeg)

###  __Sincronizarea semnalelor in GSM:__

* __Semnal de sincronizare introdus in fiecare cadru/simbol suplimentar__
  * Se introduce un simbol in fiecare cadru, care sa poata fi recunoscut usor la RX dintre celalalte simboluri: se verifica semnalul RX bit cu bit, pana se identifica semnalul de sincronizare.
  * Se stabileste un timp de intrare in sincronism __Tis = N1 x N2 = N/2 * 2N = N^2__
    * __N1__ = nr mediu al bitilor verificati inaintea identificarii simbolului de sincronizare
    * __N2__ = nr mediu de biti necesari pt a determina faptul ca simbolul analizat nu este semnal de sincronizare
    * __N__ = nr total de biti dintr-un cadru


* __Sincronizarea de cadru prin intermediul simbolurilor imprumutate__
  * Folosita pt marirea vitezei de transmisie
  * Se inlocuieste in mod periodic o secventa de biti din cadru cu o secventa care determina sincronismul de cadru

![3](C6_03.jpeg)



* __Sincronizarea realizata cu un cadru simplimentar__
  * La receptie se recunosc foarte usor: probabilitatea sa existe un cadru intreg cu aceeasi biti este mica.

![4](C6_4.jpeg)
  * Cea mai mare unitate de timp din sistemul de transmisiuni GSM este __hipercadrul__.
  * Un hipercadru are __2048__ de supercadre.
  * Supercadrul poate avea __51__ sau __26__ de multicadre:
    * 51 --> multicadrul are 26 cadre
    * 26 --> mulitcadrul are 51 cadre
  * Un cadru TDMA = 8 ferestre temporale

* __Structura unui cadru TDMA__
  * __De info__
  * __Corectie de frecventa__
  * __Corectie de sincronizare__
  * __Informatie de acces__

![5](C6_5.jpeg)


### Canalele radio utilizate in GSM
* Accesul multiplu pe interfata radio GSM este o combinatie FDMA+TDMA.  
* In banda de 225MHz pt fiecare sens de transmisiune se realizeaza, prin diviziune in fq, 124 de canale de cate 200KHz, avand fq centrale:
  * __Uplink__: 890.2, 890.4, ..., 914.8 MHz
  * __Downlink__: 935.2, 935.4, ...,, 955.8 MHz
* Se observa ca la marginile fiecarei benzi de 25MHz e lasata o banda de garda de 200KHz
* Prin TDMA, fiecare canal radio e utilizat multiplu, cu un factor de multiplexare de 8 (Pe ficare canal radio, se utilizeaza 8 canale cu diviziune in timp).
* Bitii unui segment vocal care dureaza 20ms se transmit in 8 pachete de cate 56b in fiecare pachet


* Canale radio folosite in GSM:


  * Pentru trafic:
    * __TCH/F__ (Trafic Channel / full rate): 13Kb/s pt voce; 12/6/3.6Kb/s pt date
    * __TCH/H__ (Half Rate): 7Kb/s pt voce; 6/3.6Kb/s pt dates
  * Pentru semnalizari:
    * __SACCH__: Slow Associated Control Channel - canal bidirectional, asociat unui canal de trafic ce transporta parametri de masura si control sau datele necesare pt a mentine legatura intre statia mobila si statia de baza.
    * __FACCH__: Fast ACCH - utilizeaza chiar canalul de trafic: in locul unor pachete cu date de utilizator, se vor transmite semnalizari care trebuie transmise rapid (handover).
      * Diferentierea Trafic/FACCH se face printr-un bit de FLAG.


  * Fara legatura cu un apel:
    * __SDCCH__: Stand Alone Dedicated Control Channel.
      * TX de la statia mobila: pt redirectionarea apelurilor si SMS-uri.
      * TX de la statia de baza: pt actualizarea localizarii


  * Pentru Modul Pasiv:
    * in starea idle/pasiva.
    * in starea 'idle' statia mobila se afla in corespondenta cu statia de baza pentru:
      * a intercepta mesajele de paging
      * monitorizarea mediului (pt alegerea statiei de baza cu semnal maxim).
      * a asculta mesajele scurte difuzate in celule
    * Canale:
      * __FCCH__: Frequency Correction Channel - (blank)
      * __SCH__: Synchronization Channel
      (blank) sunt ascultate si in timpul apelului pt masuratori in vederea unui eventual transfer de la o BSS la alta.
      * __BCCH__: Broadcast Channel
        * Sunt difuzati param spefici necesari statiilor mobile pt a identifica reteaua careia ii apartine celula
        * Folosit pt accesul in retea (ii trebuie codul de identificare al operatorului, etc)
      * __PAGCH__: Paging and Acces Grant Channel
        * Canal pentru paging: (blank) si e informata si ce canal i s-a alocat
      * __RACH__: Canal pt acces aleatoriu
        * statia mobila transmite pe acest CH cererile de acces in retea (sunt posibile coliziuni ale acestor cereri)


  * Pentru difuzarea mesajelor scurte in celula
    * __CBCH__: Cell Broadcast Channel
      * pt difuzare functioneaza de la statia de baza la statia mobila

* La randul sau, un canal fizic e folosit pt difuzarea mai multor canale logice. Pt ca transmisiunea este duplex, separarea sensurilor se face prin decalaj intre TX si RX cu 3 intervale temporale:
![6](C6_6.jpeg)
* In plus fata de clasificarea cu canalele de trafic in modul pasiv si pentru semnalizari, o alta clasificare este intre __canale dedicate__ si __canale comune__.


* __Canale comune__:
![7](C6_7.jpeg)
  * Multicadru de 51 cadre
  * Pt downlink avem: FCCH, SCH, BCCH, PAGCH si CBCH
  * Pt uplink avem: RACH/F s RACH/F
  * 36 cadre de paging, 4 de BCH, etc



* __Canale Dedicate__
![8](C6_8.jpeg)
* Din cele 26 de intervale, unul singur este SACCH(12), si unul singur IDLE (25). Pe un canal radio sunt intotdeauna 8 canale fizice
* Pentru a se evita ca in statia de baza mesajele de tip S sa fie receptonate simultat de la toate statiile mobile (<-> coliziuni) pentru cadrele adiacente se face un decalaj: unui canal SACCH i se aloca cadrul 12, si liber este 25, iar in cadrul adiacent, invers.
* Exista si urmatoarea structura alternativa:
![9](C6_9.jpeg)

__Concluzie__: Canalele din GSM pot fi sintetizate in urmatorul tabel:
![10](C6_10.jpeg)

---
## Curs 7 21.03.19

### __Protocoale de semnalizare in GSM__
__Arhitectura sistemului de semnalizare in GSM__
* GSM are o stiva incompleta fata de OSI, in sensul ca nu are nivelele de transport, sesiune si prezentare, iar in unele echipamente (statie mobila si BTS), nici nivelul retea.
* Nivelele principale in GSM sunt: 1, 2, iar pe nivelul 3 OSI este deja nivelul aplicatie (3_GSM = 7_OSI)


* __Nivelul fizic__
    * _Interata Radio (Um)_
        * se asigura transferul informatiei analogice digitalizate, suportul fizic fiind canalele temporale de pe purtatoarea radio.
    * _Interfata Abis (intre BTS si BSC)_
        * se bazeaza pe canalele temporale de 64kbps, suportul fizic fiind asigurat de cablu coaxial, fibra optica sau un link de microunde.
    * _Interfata A (intre bsc si msc)_
        * interfata intre elementele de reta fixa, bazata pe canale digitale de 64kbps, si protocolul la aceasta interfata este MTP1


* __Nivelul LD__
    * _Interfata Um_
        * Foloseste protocolul LAPDm (Link Acces Protocol for Data Modified)
    * _Interfata Abis:_
        * Protocolul LAPDm
    * _Interfata A:_
        * Protocolul: MTP-2 sau  X.25


* __Nivelul Retea__
    * __[!]__ La interfetele Um si Abis nu exista nivelul retea __[!]__
    * La interfata A:
        * protocolul MTP-3 si SCCP (Singalling Connection Control Part)
        * MTP3 asigura serviciu pe baza de __datagrame__: transmisiune fara confirmare, nu neaparat in ordine.
        * MTP3 + SCCP pot lucra cu/fara conexiune.


* __Nivelele superioare ale protocoalelor de semnalizare__
  * Trebuie sa consideram sistemul GSM descompus in 3 subisteme:
    * Un subsistem la interfata cu retelele fixe (PSTN si ISDN)
    * Un subsistem ce cuprinde centrele de comutatie si bazele de date (HLR, VLR, EIR, MSR si AUC)
    * Partea cu hardware specific GSM si sistemele periferice asociate: interfetele cu MSC-ul, BSC-ul, BTS-ul si MS-ul.


  * __Subsistemul la interfata cu retelele fixe__
    * protocoalele din aceasta categorie nu sunt specifice GSM-ului, ele se ocupa cu tratarea apelurilor
    * exemple de protocoale: TUP (Telephonic User Part - doar apeluri telefonice PSTN), ISUP (ISDN User Part - pt aplicatii ISDN)
    * protocoalele le gasim intr-un MSC de intrare (GMSC - Gateway MSC)


  * __Nivelele superioare in centrele de comutatie si statia mobila__
    * 3 subnivele:
      * Managementul apelurilor (CM - connection management)
      * Mobility Management (MM) - subnivel intermediar
      * Radio Resources Management (RR) - subnivelul inferior


  Toate protocoalele din subnivelul aplicatie se mai numesc in GSM si RIL3 (Radio Interface Layer 3)


  * __Subnivelul CM (Connection Management)__
    * Functiile CM exista numai in MS si MSC si include:
      * componente nespecifice GSM-ului, regasite si in PSTN, cum ar fi: Call Control (CC) de intrare/iesire, Suplementary Services (SS - controlul serviciilor suplimentare)
      * specific GSM-ului: serviciul SMS: short message service


  * __Subnivelul MM (Mobility Management)__
    * Exista doar in MS si MSC
    * Asigura managementul pt statia mobila, autentificare si servicii de management al conexiunilor pentru transportul mesajelor de la subnivelul CC.


  * __Subnivelul RR (Radio Resource management)__
    * Sunt implicate MS, MSC, BTS si BSC
    * Obiectivele protocoalelor de pe nivelul RR: stabilirea conexiunilor, transferul datelor si eliberarea conexiunilor dedicate.
    * Protocoalele de pe acest subnivel sunt denumite __RIL3-RR__
      * Ele stabilesc conexiuni intre MSC si BSC pe durata unui apel, indiferent daca statia mobila se deplaseaza sau nu
      * Difuzeaza informatii cand MS este _idle_.
      * Asigura schimbul de canale intra/inter-celule
    * Din cauza ca functiile RR sunt aplasate neuniform in echipamente -> exista 3 subprotocoale pt fiecare dintre cele 3 interfete:
      * __*Um* : RIL3-RR__
        * Numai anumite mesaje RIL3-RR sunt interpretate, majoritatea fiind transportate transparent in BTS
      * __*Abis* : RSM (Radio Subsystem Management)__
        * RSM contine functii pe care RIL3-RR nu le are:
          * functii de control ale BTS-ului de catre BSC
        * Este necesara conversia _RIL3-RR <-> RSM_
      * __*Interfata A* : BSSMAP (BSS Management Application Part)__
        * Transporta mesaje specifice nivelului 3 intre BSC si MSC
        * Se face conversia RIL3-RR <-> BSSMAP la nivelul interfetei


  * Tratarea aplicatiilor mobile in partea centrala a sistemului:
    * Protocolul MAP - mobile application part - nivelul superior
    * Protocolul TCAP - Transaction Capabilities Application Part, subnivel al MAP


![1](C7_01.png)
// DTAP: Direct Transfer Application Part


__Asigurarea securitatii comunicatiilor in GSM__  
* Functiile de securitate in GSM au 2 scopuri:
  * Protejarea retelei impotriva accesului neautorizat
    * Protejarea abonatilor de folosirea frauduloasa a identitatii lor
  * Protejarea confidentialitatii convorbirilor abonatilor


* Protejarea retelei impotriva accesului neautorizat se face prin autentificare <-> verificarea faptului ca identitatea abonatului corespunde cu cea a SIM-ului inserat. Functia este importanta mai ales in cazul roaming-ului (reteaua vizitata nu poate controla idendtitatea abonatului)
* Protejarea abonatilor de folosirea frauduloasa a identitatii lor si protejarea confidentialitatii convorbirilor abonatilor se face prin *transmisie criptata* si prin inlocuirea idenditatii abonatului cu numarul de identitate temporar intre MSC si VLR


* __Autentificarea__
  * Prin PIN <-- e posibila interceptarea emisiei
  * In GSM se utilizeaza codul PIN in conjunctie cu SIM-ul: PIN-ul e verificat in SIM fara sa fie transmis in interfata radio.
  * Cea mai sigura metoda: calculul raspunsului unei functii avand ca parametru cheia secreta personala K_i


  ![2](C7_02.png)
* A3 foloseste in spate COMP128
  * e o functie greu inversabila
  * RAND in [0; 2^128-1] --> 128b
  * SRES are 32b
  * Ki poate avea orice format si orice lungime(<=128b)


* __Criptarea__
  * folosita pentru protejarea datelor in modul cifrat, a semnalizarilor utilizatorului, dar si a semnalizarilor sistemului
  * Algoritmul A5 genereaza o secventa de 114b din ceia de criptare si o cheie temporara formata din indexul cadrului.
  * Criptare prin XOR intre 114b ai secv de criptare si datele ce trebuie criptate
  * Cheia Kc e stabilita intre MS si retea in timpul procesului de autentificare
  * S1/S2 --> 2 secvente, una pt Tx una pt Rx (nr poate diferi intre sensuri, s-ar desincroniza indicii)
  * A5 trebuie specificat la nivel international, pt a obtine facilitatile de roaming; trebuie implementat in fiecare BS si MS.
  ![3](C7_03.jpeg)


####  __Stabilirea apelurilor in care statia mobila este apelata__
* In cazul in care statia mobila este dirijata, apelul este dirijat catre MSC de catre PSTN (a identificat ca apelul este adresat unui utilizator mobil).
* Trebuie interogat HLR-ul, care va da identitatea VLR-ului care controleaza zona in care se afla abonatul chemat
* HLR-ul solicita VLR-ului sa ii comunice numarul MSRN
* MSRN-ul permite dirijarea apelului catre MSC-ul din zona in care se afla abonatul mobil.
* Comutatorul MSC de destinatie va receptiona apelul si il va transmite VLR-ului care identifica zona de localizare --> sunt transmise semnalele de paging pt stabilirea celulei in care se afla mobilul.

#### __Transferul legaturii de comunicatie (Roaming)__
![3](C7_04.jpeg)
* Intr-o retea mobila sunt posibile mai multe tipuri de transfer:
  * 1a, 1b:
  * Intre sisteme de statii de baza diferite
  * Intre sisteme de baza diferite si subordonate unor centre de comutatie diferite


* Transferuri:
  * de baza (basic handover): intre MSC-ul care a initiat convorbirea si un al doilea MSC
  * ulterior: procedura de transfer este apelata pentru 2 MSC-uri, altele decat cele care au avut initial controlul convorbirii, sau pentru cazul in care mobilul revine in zona acoperita de MSC-ul initial.


* __Transferul de baza__
  * pe baza masuratorilor, se stabileste noua statie de baza care sa preia legatura.
  * MSC-ul va transmite un mesaj de transfer catre noul MSC prin care il informeaza si despre caracteristicile noului canal radio.
  * Noul MSC raspunde cu un mesaj de confirmare, dar numai dupa ce a verificat numarul MSRN al mobilului din VLR.
  * Urmatoarea etapa: transmiterea mesajului de confirmarea impreuna cu identitatea canalului, si abia dupa ce s-au parcurs toate aceste etape, MSC-ul initial stabileste conexiunea cu noul MSC.
  * Ultima etapa: primul MSC elibereaza canalul radio cand primeste semnalizarea de eliberare de la al 2lea MSC.


  ---
  ## Curs 8 27.03.19

__Situatia in care MS s-a deplasat din celula A in celula B__   

![1](C8_01.jpeg)

* Daca intre MSC-A si MSC-B nu se poate stabili legatura (e.g. in locul msj complet pt adresa, se primeste unul de eroare), atunci MSC-A inceteaza efectuarea procesului de handover, fara e elibera insa canalul radio folosit.
* MSC-A are controlul asupra convorbirii pana cand abonatul renunta la convorbire. --> MSCA transmite un _semnal de incheiere_.
* __Obs__: MSC-A poate incheia in orice moment procedura de handover prin transmiterea _semnalului de incheiere_

__Subsequent handover (transfer ulterior)__ _= transfer cu 2/+ MSC-uri sau cu intoarcere in prima celula_

* Mai intai MSC-B transmite catre MSC-A un mesaj prin care se indica trecerea statiei intr-o noua celula: mesaj prin care se idnica trecerea statie intr-o noua celula, specificnad ca aceasta noua celula e controlata de MSC-A
* MSC-A, fiind centrul de comutatie initiator al apelului, are controlul legaturii, nemaifiind necesara atribuirea unui numar de deplasare.
* MSC-A va cauta un canal radio pt a prelua legatura si comunica canalul liber MSC-B, initiind procedura de transfer intre cele 2 MSC-uri.
* Daca MSC-A nu gaseste niciun canal liber, MSC-B va mentine un timp cat mai lung posibil legatura cu statia mobila.-

![2](C8_02.jpeg)


### Formarea apelurilor de iesire de la statia mobila
![3](C8_03.jpeg)

* Diferente fata de retele fixe:
  * Numarul este introdus de reteaua mobila inainte de a accesa reteaua
  * La inceput se schimba intre statia mobila si retea informatii aditionale (e.g. tipul serviciului, canalului)
  * MS este un echipament inteligent si actioneaza ca un translator al comenzilor abonatului la protocolul __RIL3-CC__ _(Radio Interface Layer 3 - Call Control)_.


* __IAM__: Initial Address Message (MSC<->Retea externa)
* __ACM__: Address Complete Message
  * __\*1__: MSC-ul poate primi de la retea un mesaj de tipul _ISUP Release_, in cazul in care reteaua externa e de tip ISDN, indicand faptul ca a esuat stabilirea legaturii cu MSC-ul. Daca nu s-a primit asa ceva, se trimite __\*2 Alerting__. Daca nu, in loc de Alerting se va trimte la MS mesajul __RIL3-CC Disconnect__ si statia mobila va raspunde cu __RIL3-CC Release Complete__.
* __Connect__ indica ca totul a mers bine, confirmand conexiunea, trecandu-se la conexiune.


* __\*3__: Cazul in care abonatul B nu raspunde si a expirat si timpul alocat asteptarii raspunsului acestuia.


### __Roaming__
__Def.__ = Capacitatea abonatului de a efectua/receptiona in mod automat apeluri vocale, de a transmite sau receptiona date, sau de a accesa servicii in momentul in care calatoreste in afara ariei de acoperire a retelei de baza.

* [Etapa 1]: In momentul in care un abonat nu a fost inregistrat in reteaua vizitata(cea in care se deplaseaza prin roaming), aceasta va trebui sa ceara date despre el de la reteaua de baza.
  * Date de autentificare si autorizare a serviciilor folosite
  * Abonatul va primi o identitate in reteaua vizitata.

__[!]__ serviciul de roaming este posibil <-> abonatul poate sa utilizeze serviciile pe care le avea in reteaua de baza.

* In procesul de roaming au loc negocieri, in primul rand in scopul taxarii abonatului si asigurare a serviciilor.
* [Etapa 2]: Reteaua vizitata contacteaza reteaua de baza si solicita informatii despre serviciile oferite abonatului (pe baza IMSI).
* Daca negocierile s-au incheiat cu succes, reteaua vizitata incepe sa retina toate informatiile despre acel abonat.

* Negocierile intre reteaua de baza si cea vizitata au la baza procese standard, denumite __IREG__ (_International Roaming Expert Group_) si __TADIG__ (_Transferred Accound Data Interchange Group_)
  * IREG are rolul de a testa functionalitatea legaturilor radio (partea tehnica, gen)
  * TADIG se ocupa de $tarifare$


* Apelurile din reteaua vizitata sunt stocate intr-un fisier __TAP__ (_Transfered Account Procedure_), iar acelasi fisier in sistemele CDMA/AMPS se numeste __CIBER__ (_Cellular Intercarrier Billing Exchange Roamer_): (chemator + chemat + locatie + ora + durata) apel



#### __Tipuri de Roaming__
  * __Regional__: Deplasarea dintr-o regiune in alta in cadrul acoperirii nationale a unui operator.
  * __International__: posibilitatea de deplasare la un nou furnizor de servicii
    * __[!]__ Probleme: benzile de fq folosite. E.g.: GSM avea 800, 1800MHz, iar SUA avea 850, 1900MHz --> MS trebuia sa fie compatibil cu toate cele 4 fq
  * __Inter-standarde__: deplasarea intre retele mobile cu tehnologii diferite



### __Retele Ad-Hoc (MANet - Mobile Ad-Hoc Networks)__

* __Def__: Retea auto-configurabila de routere mobile, conectate prin linkuri wireless dupa o topologie oarecare.
* Topologia e dinamica: se poate modifica rapid, imprevizibil.
* Use-cases: campusuri universitare, medicina, armata, misiuni de explorare

* Exista 3 tipuri de protocoale:
   * __Proactive__: asigura routing pe baza unor tabele de dirijare si asigura routing independent de cerintele de trafic
    * __[-]__ Dezavantaj: nodurile actualizeaza in mod continuu imaginea retelei --> reactioneaza la schimbarile de topologie chiar daca traficul nu este afectat.
   * __Reactive__: stabilesc rute intre noduri numai cand trebuie sa se schimbe un schimb de pachete de date --> mai eficiente
    * La solicitarea unei rute se incepe un proces de descoperire a rutei de la nod la nod
   * __Hibride__

* Avantaje retele ad-hoc:
  * Costuri mai mici
  * Instalare rapida
  * Performante mai bune


* Dezavantaj:
    * Management dificil al retelei
        * decentralizat (fara supervizor)
        * topologie  in continua schimbare


* __Protocolul Bluetooth__
    * Conecteaza intre ele diferite echipamente fara a utiliza cablu sau orice alt mediu ghidat.
    * Legaturile functioneaza pe principiul Master/Slave, un Master putand controla cel mult 7 module din zona lui.
    * __Piconet__ = Master + 1..7 Slaves


* Clase de echipamente bluetooth:
    * __1__:
        * P_Tx < 100mW
        * d < 100m
    * __2__:
        * P_Tx = 1..2.5mW
        * d < 10m
    * __3__:
        * P_Tx < 1mW
        * d = 0.1 ... 10m


* Arhitectura Bluetooth:
    * __Radio__
        * zona 2.45GHz
        * max v_tx = 1Mbit/s
        * se foloeste si aici _Frequency Hopping_, implementat cu __GFSK__ (_Gaussian Frequency Shift Keying_)
    * __Frecvente de baza__
        * foloseste o combinatie a tehnologiei de comutatie a circuitelor cu tehnologia de comutatie a pachetelor
        * in BT se poate asigura 1 canal de date asincron si pana la 3 canale de voce simultane sincrone.
        * _sau_ un canal simultan: date asincrone si voce sincrona
    * __Controllerul legaturii de date (Link Manager Protocol)__
        * Configureaza, autentifica si specifica modul de utilizare a conexiunilor dintre echipamentele BT.
        * Controleaza _modul de consum redus de putere_.


#### __Moduri de consum redus de putere:__
* __Modul activ__: dispozitiul BT participa activ la transmisie
    * Consum de 40-50mA
    * Sincronizare: master trimite periodic un semnal de polling catre modulele slave. slave->master trimit si ele periodic pt resync.


* __Modul de veghe (sniff)__:
    * Consum de 1-5mA
    * Modulul BT nu este activ, ramane sincronizat in piconet
    * Urmareste transmisiile facute in retea, la intervale regulate, dar pe durate mici de timp.


* __Modul pasiv (hold)__:
    * Modulele raman sincronizate, dar din tot echipamentul BT ramane activ doar _Link Manager_-ul


* __Modul park (repaus)__:
    * Echipamentul BT nu mai e membru activ al retelei, dar ramane sincronizat cu Master-ul.


* Intr-o retea piconet Master-ul este primul care se conecteaza si el este cel care seteaza ceasul si secventa de salt de fq si codul de acces al link-ului.
* Toate modulele Slave folosesc aceeasi secventa de salt de fq si sunt sincronizate cu ceasul Master-ului.
* Rolul fiecarui modul este invizibil pt un utilizator (user nu stie daca e Master/Slave).
* Un echipament poate apartine mai multor retele piconet, cu functii diferite (intr-o retea master, in alta slave)
* Mai multe retele piconet care se intersecteaza formeaza un __scatternet__. Datorita faptului ca se folosesc pana la 10 piconet-uri, 80 de module ((7+1) \* 8 ) pot transmite la capacitatea maxima.

* Tipuri de conexiuni in BT:
    * Conexiuni sincrone orientate
        * Canale folosite pt voce
        * Pot exista maxim 3 astfel de canale, un slave putand controla 2 canale de la mastere diferite
    * Conexiuni asincrone fara orientare
        * Asigura tx asincrona Master<->Slave numai pentru date


#### Securitatea Comunicatiei in BT
* Functioneaza intr-o retea ad-hoc. In sistemele distribuite, autentificarea este mai dificila.
* Autentificare locala sau pseudo-centralizata?


* La autentificare oarecum centralizata:
    * Bloc __Key Distribution Center__  --> se memoreaza cheile tuturor echipamentelor.


* La securitate asigurata local:
    * Se creeaza centrul __Certification Authority Center (CA)__ --> emite certificate de chei publice de autentificare
    * __Certification Distribution Center (CDC)__ --> memoreaza toate certificatele eliberate de CA
    * Userii au propriile chei de autentificare, pe care le pot certifica cu CA. Daca un user foloseste o cheie pt a accesa un echipament, ea este verificata in ce masura corespunde unei chei publice. La randul ei, cheia publica e verificata in CDC, pentru a se dovedi daca apartine utilizatorului care a dobandit-o initial.

---
## 28.03.19 Curs 9

#### __Securitatea Echipamentelor BT__
* In fiecare echipament BT exista urmatoarele elemente:
    * __Adresa echipamentului (48b)__, unica pt fiecare echipament BT.
    * __Cheie privata de criptare__
    * __Rand (128b)__, generat de echipamentul BT.


* Modurile de realizare a securitatii:
    * __1__: nesecurizat
    * __2__: securizare la nivel servicii
    * __3__: securizare la nivelul legaturii de date
        * __2 vs 3__: in modul 3 echipamentul BT initiaza procedurile de securizare inainte de stabilirea canalului.


* Securizare a echipamentelor, 2 niveluri:
    * nesecurizat
    * securizat: are acces la toate serviciile


* Securizare a serviciilor:
    * Servicii cu autorizare si autentificare
    * Servicii care necesita numai autentificare
    * Servicii disponibile tuturor echipamentelor


* Managementul cheilor de autentificare
    * toate tranzactiile care sunt efectuate pt realizarea securitatii sunt realizate prin intermediul cheii legaturii de date (__LK__ - _link key_, numar aleator de 128b)
    * __LK__ va fi folosit mai departe ca parametru in obtinerea cheii de criptare. Spre deosebire de cheia de autentificare de la GSM, LK are o anumita durata de viata: semipermanenta sau temporara.
        * Cheia semipermanenta poate fi folosita si dupa incheierea sesiunii curente de autentificare.
    * Mai exista _chei combinate_: derivate din informatiile care se obtin de la 2 echipamente BT si se genereaza pt fiecare pereche nou-formata de echipamente.
    * __Cheia individuala__: generata intr-un singur echipament, la instalarea lui.
    * __Cheia principala__: cheie temporara care inlocuieste __LK__, folosita cand echipamentul trebuie sa transmita o informatie la mai multi destinatari.
    * __Cheia de initializare__: folosita ca __LK__ in timpul procesului de initializare.
        * generata pe baza codului PIN (din BT, lung de 1-16octeti)


//http://www.rfwireless-world.com/Tutorials/Bluetooth-security.html

__Criptare - Algoritmul E0:__
![1](C9_01.gif)  
Rezultatul final este __Kstr__, cu care se face _xor_ pe datastream.

* Exista mai multe moduri de criptare, in functie de cheia folosita de un echipament:
    * Daca se utilizeaza chei ?? sau combinate, traficul de date nu e criptate.
    * cheie principala --> 3 moduri de criptare:
        * __1__: nimic criptat
        * __2__: traficul de difuzare nu e criptat (punct->multipunct), iar traficul de adresare individuala e criptat cu cheia principala
        * __3__: tot traficul e criptat cu cheia principala.

__Autentificare - Algoritmul E1:__
![2](C9_02.gif)  

 * Daca autentificarea esueaza, ea va fi reluata dupa o perioada de timp, dublata dupa fiecare incercare nereusita, pana cand expira un anumit timp de asteptare.


 #### Probleme in asigurarea securitatii in  BT:
* Algoritmul E0, de criptare, este _sensibil_.
* De cate ori se conecteaza 2 echipamente, trebuie introdus codul PIN la fiecare. <-- PIN parametru pt cheia de criptare, nu trebuie sa aiba lungimrea prea mici
* Autentificarea si criptarea se bazeaza pe __LK__. Luam urmatoarea situatie:
    * Echipamentele A si B utilizeaza ca LK cheia individuala a lui A.
    * Modulul C comunica cu A si foloseste ca LK tot cheia individuala a lui A. --> echipamentul B poate folosi cheia individuala a lui A pt a calcula cheia de criptare si sa intercepteze traficul dintre A si C. B poate face _man in the middle_.
    * [!] LK e transmis in clar (read up online)


### Telefonia cordless
* Au existat mai multe sisteme de telefonie:
    * CT2 (Cordless Telephony 2)
    * DECT (Digital European Cordless Telecom) <-- folosite actual
    * PACS (Personal Acces Comm System)
    * PHS (Personal Handyphone System)

![3](C9_03.jpeg)  


* CT2 are banda de fq:
    * pe sensul BS->MS: 959-960MHz
    * MS -> BS: 914-915MHz
* Distanta intre canale: 25KHz
* Nr canale: 40
* Puterea maxima radiata: 10mW
* Raza zonei de serviciu: 50m indoor, 200m outdoor


* Ocuparea canalelor radio poate fi initiata si de echipamentul mobil, si de cel fix.
* Presupunem ca echipamentul mobil initiaza convorbirea:
    * se incepe cu cautarea unei perechi de canale libere
    * se transmite periodic codul propriu de Identificare
    * echipamentul fix identifica acest cod si inceteaza baleierea canalelor.
    * echipamentul fix isi transmite codul, dupa care se incepe formarea numarului, urmand convorbirea.
    * convorbirea se incheie prin faptul ca unul dintre cele 2 echipamente transmite un mesaj de incheiere, incluzand si codul lui propriu.


* Sistemul DECT - noua denumire: Digital Enhanced Cordless Telecomm
    * furnizeaza o tehnologie de acces radio pt telecomunicatiile fara fir
    * opereaza in banda 1880-1900MHz, utilizeaza modulatie GMSK
    * utilizeaza structuri picocelulare (20-200m), asigurand servicii cu costuri mai reduse decat CT2.
    * Diviziunea spectrului in fq: 10 purtatoare cu TDMA

![4](C9_04.jpg)  
* Nivelul fizic:
    * Modulare/Demodulare
    * Mentinerea sync intre emitatoare si receptoare
    * Activarea si dezactivarea canalelor fizice la cererea MAC
    * Receptionarea in statia mobila a tuturor canalelor fizice (statia mobila nu trebuie sa vada care apeluri ii sunt destinate)


* Nivelul OSI2 - LD:
    * Subnivelul MAC
        * Aloca canalele radio
        * Multiplexeaza canalele logice (control, identificare, etc) pe canale fizice
        * Adaptarea fluxurilor de date de nivel superior la format utilizabil pe transmisia prin mediul fizic
        * protectia la erori
    * Subnivelul DLC
        * impartit intre C-Plane si U-Plane
        * in U-Plane se asigura diferite servicii cu/fara conexiune
        * in C-Plane se asigura transferul informatiilor de semnalizare


* Nivelul OSI3 - Retea:
    * realizeaza controlul apelurilor (fctii specifice pt stabilirea, mentinerea si desfiintarea conexiunilor)
    * gestiunea serviciilor aditionale: redirijarea apelurilor, managementul taxarii
    * gestiunea mobilitatii: identificare + localizare
    * controlul legaturilor de date


* Caracteristici ale sistemului DECT:
    * asigura o calitate foarte buna a semnalului vocal
    * se asigura un consum redus de energie prin utilizarea unei transmisii discontinue
    * compatibilitatea statiei de baza si a statiei mobile DECT pt diferiti fabricanti
    * nivel acceptabil de securitate
    * interconectarea cu alte retele
    * fata de CT2, asigura handover
    * se asigura si acoperire cu suprapuneri (straturi multicelulare)


![5](C9_05.jpeg)  
* ^Obs figura: In loc de FP, notam BTS
* GP folosit pt anularea *offset(clock\_tx, clock\_rx)*
* Header -> sync biti
* Syncro -> sync cuvant
* CRC(4b) contin suma contorului celor 320b de informatie

* Un cadru temporal poate avea:
    * 24 intervale temporale intregi (full slots, cu 480b fiecare)
    * 48 intervale temporale 1/2 (half slots, cu 240b fiecare)
    * 12 intervale temporale \*x (double slot, 960b fiecare)
    ![6](C9_06.jpeg)  


* DECT foloseste 4 tipuri de pachete fizice
    * P00 - scurt
    * P32 - de baza
    * P08j - de capacitate scazuta
    * P80 - de capacitate mare


* Toate statiile de baza si mobile pot Rx/Tx pachetele fizice scurte, sau sa Tx/Rx cel putin unul dintre celelalte.
* Indiferent de tipul pachetului, exista 3 campuri:
    * S - sync
    * D - date
    * Z - detectie coliziuni/erori

---
## 04.04 Curs 10
* Pentru __Pachetul P00__ (96b):
    * 32b sincronizare
    * 64b date


* Pentru __Pachetul P32__ (424b):
    * 32b sincronizare
    * 388b date
    * 4b campul _Z_

* Pentru __Pachetul P08j__ (184b):
    * 32b sincronizare
    * 148b date
    * 4b corectie

* Pentru __Pachetul P80__ (904b):
    * 32b sincronizare
    * 868 date
    * 4b corectie

__Retelele HSCSD ( High Speed Circuit-Switched Data)__
* permit alocarea in comun pt un utilizator a mai multor canale de trafic intr-o configuratie speciala
* __Scop__: Posibilitatea oferirii unei multitudini de servicii cu viteze de transmisie diferite la interfata radio.
* Se pot obtine viteze mai prin imbunatatirea transmisiei la interfata radio (scheme diferite de codare & modulare).


* Stabilirea unei conexiuni HSCSD
    * negocierea unor parametri intre utilizator si retea:
        * viteza de transmisie a utilizatorului
        * modul de codare a canalului pt un apel
        * numarul maxim de canale pe care il poate accepta utilizatorul
        * viteza de transmisie pe care o doreste abonatul la interfata radio
        * indicatii despre modificarile solicitate de abonat


![pic1](C10_01.jpeg)


* Atat in retea cat si la statia mobila trebuie sa existe niste functii definite pentru combinare / impartirea datelor in fluxuri separate.
* Fluxurile sunt apoi transmise din interfata radio prin intermediul a _n_ canale. Dupa divizare, fluxul de date e transportat de catre cele _n_ canale de trafic, ca si cum ar fi independente unul de celalalt, pana in punctul in care sunt recombinate (la BSC sau la MS in fct de sensul de comunicare).
* Cele _n_ canale sunt privite in retea ca o singura legatura radio.

#### __Generatia 2.5G__
__GPRS__ (General Packet Radio Services)
* Avantaje fata de GSM:
    * debite de date mai mari la interfata radio.
    * folosirea eficienta si partajata a resurselor radio de catre mai multi utilizatori.
    * timpi de acces mult mai mici decat in GSM
    * taxarea se face dupa volumul datelor transferate, nu dupa durata conexiunii.
    * GPRS coexista cu GSM si foloseste si canalele temporale ale interfetei GSM


![pic2](C10_02.jpeg)
* Interfetele notate cu _G_: specifice GPRS.
* noduri noi: SGSN, GGSN
* __GSN__: GPRS Support Node
    * __SGSN__: Serving GPRS Support Node
        * rutarea pachetelor de la/inspre statiile mobile
        * managementul mobilitatii
        * autentificarea si criptarea
        * furnizarea informatiilor pt taxare
        * memorarea informatiilor pt localizare (celula, registrul VLR)
        * profilul abonatilor (IMSI)
    * __GGSN__: interfata intre reteaua GPRS si retelele de date cu comutatie de pachete externe.
        * transforma pachetele GPRS sosite de la _SGSN_ in formatul protocolului de pachete de date din reteaua externa (notat generic _PDP_: packet data protocol)
        * transfera pachetele intre retele
        * info de taxare
        * functii de autentificare


* La un __GGSN__ se pot conecta mai multe __SGSN__-uri, si invers.
* Exista 3 tipuri de MS:
    * clasa *A*
        * MS poate avea o conexiunea GSM simultan cu un transfer de pachete.
    * clasa *B*
        * MS poate fi atasata simultan si la GSM si la GPRS, dar nu poate folosi ambele servicii in acelasi timp
        * MS poate intrerupe transferul pachetelor pt a receptiona un mesaj GSM, dupa care reia legatura GPRS.
    * clasa *C* :
        * MS poate fi atasata numai la unul din cele doua tipuri de servicii (exceptie: SMS, pot fi receptionate si transmise oricand)


* Un MSC are o arie de acoperire notata _LA (Location Area)_ . LA e formata la randul ei din mai multe _RA_ (Routing Area). _RA_ sunt formate din mai multe celule.        
* HLR devine o baza de date comuna GSM/GPRS.
* Tripleta de autentificare (Rand, Sres, K_i) se transmite direct de la HLR la SGSN, fara sa mai treaca prin MSC si VLR.
* Celelalte registre raman la fel, doar ca trebuie sa aiba si informatiile despre GPRS.
* In MS si BTS apare un nou bloc: unitatea de codare a canalului (CCU)
    * In afara de codare si decodare, realizeaza si masuratori pe canalul radio.
    * Tot in BSS se face separarea GSM/GPRS
* in BSC apare unit de control a pachetelor (PCU):
    * aloca 8 sloturi temporale, independente pe uplink / downlink


* Proceduri de apel si actualizare in
    * Atasare si activare a contextului PDP:
        * atasare IMSI
        * atasare GPRS (in 2 etape)
            * atasare propriu-zisa GPRS
            * activarea contextului PDP


* In figura: numerele = etape de atasare in GPRS
* Se presupune ca MS s-a deplasat intr-o noua arie, ce are alt _SGSN_.  
![pic3](C10_03.jpeg)


* Atasarea GPRS:
    1. MS trimite o cerere de atasare catre SGSN
    2. Daca MS nu e cunoscuta de SGSN, el va solicita IMSI si tripleta de autentificare vechiului SGSN (_old_ in figura)
    3. Daca vechiul SGSN nu are inregistrata MS, va trimite un mesaj de eroare noului SGSN, si noul SGSN va solicita numarul IMSI statiei mobile.
    4.  SGSN autentifica statia mobila
    5. Se autentifica HLR, doar daca SGSR face parte dintr-o alta arie de serviciu
    6. se actualizeaza (BLANK)
    7. SGSN va comunica MS numarul temporar


* Obs: E posibil ca atasarile GPRS si GSM sa fie realizate simultan.
* Obs: Detasarea MS de la retea poate fi initiata fie de MS fie de retea, si implica un schimb de informatii intre MS, BSS, SGSN si GGSN.

* Activarea contextului PDP:
    * Dupa atasarea GPRS, statiei mobile i se vor atribui 1/+ adrese de tipul celor utilizate in reteaua externa (adrese PDP).
    * Pt fiecare sesiune de comunicatie se creeaza un contet PDP care descrie caracteristicile sesiunii:
        * un context PDP descrie tipul PDP, adresa PDP alocata statiei mobile QoS-ul cerut si adresa unui GGSN.
    * Activarea contextului PDP e echivalenta cu inregistrarea MS in reteaua externa cu comutare de pachete si permite sa inceapa sa transmita si sa receptioneze datele
        * deosebire fata de GSM: in GPRS utilizatorul poate activa simultan mai multe contexte PDP, cu conditia ca terminalul sa suporte mai multe adrese IP.


![pic4](C10_04.jpeg)
    * TID = Tunnel ID

* Etapele atasarii PDP:
    1. MS transmite catre SGSN cererea de activare a contextului PDP
    2. Intre MS si SGSN se executa functiuni de securitate
    3. SGSN valideaza cererea de validare a contextului PDP
    4. SGSN verifica abonamentul si QoS (pentru taxare) cerute de user. Se creeaza o identitate a tunelului (TID) si o legatura logica catre GGSN numita GTP (GPRS Tunneling Protocol)
    5. GGSN contacteaza reteaua externa si ii cere retelei externe o adresa IP
    6. Serverul din reteaua externa transmite adresa IP catre GGSN
    7. GGSN trasnmite adresa IP catre statia mobila


* Obs: dezactivarea contextului PDP poate fi initiata de MS, SGSN sau GGSN
* Obs: alocarea adreselor PDP poate fi statica/dinamica.
    * In cazul alocarii statice, operatorul retelei la care s-a inregistrat abonatul, ii atribuie permanent o adresa PDP.
    * In cazul alocarii dinamice, adresa ii este atribuita dupa activarea contextului PDP, fie de operatorul in care s-a inregistrat initial abonatul, fie de operatorul retelei in care se afla in vizita abonatul. Alegerea intre cele doua situatii este facuta intotdeauna de catre operatorul initial.

#### __Proceduri de actualizare a localizarii propriuzise__
* Pentru a se tine evidenta localizarii curente a unui abonat, statia mobila transmite frecvent mesaje catre SGSN, aparand doua situatii:
    1. Daca mesajele sunt trimise prea rar: nu se mai cunoaste pozitia exacta (celula in care se afla mobilul) --> trimitere semnale de paging care ar conduce la intarzieri mari
    2. Daca mesajele sunt trimise prea des: localizare si latency foarte bune, dar apare congestie inutila in retea


![pic5](C10_05.jpeg)
* Statia este in una din stari in functie de marimea traficului.
* Frecventa acutalizarilor e diferta in functie de starile in care se afla statia.


---
## 10.04.19 Curs 11 [BLANK]
[Blank]

---
## 11.04.19 Curs 12
### Protocoalele de semnalizare in GPRS
* atasarea si detasarea in __GPRS__
* activarea contextului PDP
* Controlul rutarii
* alocarea canalelor Radio


__Planul de semnalizare SGSN__  
![pic1](C11_01.jpeg)


GMM/MS Functii:
* atasare/detasa GRPS
* Securitate
* Activare context PDP
* actualizarea ariei de rutare

__Planuri de semnalizare: SGSN-HLR, SGSN-EIR, SGSN-MSC/VLR__  
![pic2](C11_02.jpeg)  
Obs: BSSAP+, "+" pentru modificarile adaugate pt GPRS

* __MAP__ transporta:
    * informatiile de semnalizare, referitoare tot la actualizarea localizarii
    * informatii de rutare
    * profilul abonatului
    * semnalizari necesare pt handover


* __BSSAP+__:
    * [blank] atunci cand e necesar sa se coordoneze functiile GPRS si GSM


__Transferul pachetelor in GPRS__
* 3 moduri de functioare a retelei:
    * __modul 1__:
        * reteaua transmite mesajele de paging GSM pt o statie care e atasata si GPRS, fie pe canalul de paging GPRS (not. PCCH), fie pe canal de trafic GPRS (ambele fiind de tipul pachet, GPRS).
    * __modul 2__:
        * reteaua transmite mesajele de paging GSM pt o statie care e atasata si GPRS, pe canalul de paging GSM (__NU__ foloseste canale de tip pachet).
    * __modul 3__:
        * reteaua transmite mesajele de paging GSM pt o statie care e atasata si GPRS, pe canalul de paging GSM, iar mesajele de paging GPRS sunt transmise fie pe canalul de paging GSM, fie pe canalul de paging GPRS. --> cazul cel mai defavorabil: MS trebuie sa monitorizeze ambele canale de paging.


* Rezulta urmatoarele situatii:  
![pic3](C11_03.jpeg)  

* In modul 1, daca exista interfata Gs, SGSN-ul va coordona procedurile de paging pe baza numarului international IMSI, daca statia se afla in starile _ready_ si _standby_.
* In lipsa interfetei _Gs (modurile 2, 3)_, mesajele de paging trec prin _interfata A_ dintre MSC si BSC si reteaua paote functiona in _modul 2_, in care nu i se aloca celulei un canal de paging GPRS, sau in _modul 3_, in care poate fi folosit canalul de paging GPRS.
* Indiferent de modul adoptat, modul de functionare trebuie sa fie acelasi in fiecare celula si trebuie transmis si statiilor mobile.


#### __Realizarea Paging-ului__
* Daca o statie se afla in starea _ready_, _SGSN_-ul va transmite datele de paging catre _PCU_ (unitatea de control a pachetelor din controllerul BSC), impreuna cu informatiile despre celula si aria de localizare.
    * Daca MS e implicata intr-un transfer de pachete, PCU va transmite mesajul de paging pe canalul de control asociat canalului de trafic GPRS (PACCH).
    * ??? , sau pe canalul de paging GSM (PCH), daca primul nu este disponibil.


* Daca MS se afla in starea _standby_, transferul de pachete e initiat de _SGSN_, care va trimite catre _PCU_ o cerere de paging, _PCU_ determina apoi grupul de paging din care face parte MS, si dupa ce a determinat grupul, va trimite cererea intr-un timeslot in care statia asculta. Dupa primirea cererii, MS->SGSN un mesaj de raspuns la paging, trecand in starea _ready_.

#### __Stabilirea unui flux de date temporar__
* __Stabilirea unui DLTBF (DownLink Temporary Block Flow)__
    * In momentul primirii de cadre LLC de la SGSN, unitatea de control a pachetelor PCU trebuie in primul rand sa verifice daca MS careia ii erau destinate e libera sau cumva implicata intr-un alt transfer.
    * 3 Situatii posibile:
        * Daca MS are deja un DLTBF, noile cadre LLC vor fi puse in coada celorlalte cadre care asteapta sa fie transmise.
        * Daca MS are un ULTBF, atunci PCU poate sa ii aloce niste canale pe downlink, care sa fie cel putin partial in aceleasi sloturi cu cele alocate pe uplink.
        * Daca MS nu e implicata in niciun transfer, i se va transmite un mesaj de alocare in sens descendent.
    * Dupa implciarea statiei intr-un transfer de pachete, ea va ramane intr-un mod de ascultare notat non-DRX (non discontinous reception), nefiind necesar sa astepte un mesaj de paging, mesajul de alocare putand sa-i fie transmis imediat.


* __Stabilirea unui ULTBF (UpLink Temporary Block Flow)__
    * daca MS nu are stabilit un TBF, va trimite catre PCU un mesaj de cerere canal de tip pachet, iar alocarea i se va face intr-o etapa sau in doua etape.
        * Alocare in o singura etapa: statiei mobile ii sunt alocate mai multe sloturi, pentru un interval de timp mai mare, prin metoda alocarii dinamice. Pentru fiecare ??
        * Alocare in doua etape:
            * mai intai i se aloca MS un interval temporal pentru a transmite un singur bloc de tip RLC
            * blocul RLC e folosit ca sa i se transmita mobilului un mesaj de cerere de canal de tip pachet.
            * ??
            * Blocurile radio pot fi transmise cu confirmare/fara, in ambele moduri sunt transmise mesaje de ACK/NACK, dar reTX se face numai pentru modul cu confirmare.
            * Motivele pentru care s-ar transmite ACK/NACK pentru modul fara confirmare:
                * sa se verifice ca nu s-a intrerupt comunicatia
                * pentru a afla care e calitatea canalului de transmisie (pentru alegerea a una dintre cele 4 scheme de codare)
                * trebuie sa se prioritizeze MS in functie de calitatea legaturii.


* __Terminarea unui TBF__
    * Daca intr-o unitate PCU nu mai sunt cadre de transmis catre o MS, se va elibera downlink TBF.
        * Obs: Daca imediat dupa eliberare PCU primeste noi cadre LLC pentru aceeasi statie, totusi PCU va transmite o noua alocare corespunzatoare unui nou TBF. Avantaj: Statia mobila fiind in _ready_, nu mai e nevoie de paging.
        * Obs: Cand MS mai are de transmis doar cateva blocuri, va semnala acest lucru retelei si va incepe un proces de contorizare. Dupa ce toate blocurile au fost transmise si s-a primit si confirmarea, TBF-ul va fi eliberat. Daca MS constata dupa initierea procedurii de contorizare ca are de fapt mai multe cadre de transmis, va trebui sa stabileasca un nou TBF, pentru ca nu i se permite sa transmita mai multe pachete decat a specificat la inceputul contorizarii.


* Daca intr-o retea GSM selectia celulei era efectuata de catre controllerul statiei de baza (BSC), in GPRS statia mobila alege statia de baza pe baza masuratorilor.   
_---(End of 2.5G)---_

#### UMTS (Universal Mobile Telecommunications System)
* Contine 2 mari componente:
    * RAN: Radio Access Network (in EU avem si _UTRAN_: UMTS Terrestial RAN)
    * CN: Core Network - realizeaza comutatia si rutarea


* UTRAN:
    * Format din 1/+ RNS (Radio Network Subsystems)
    * Un astfel de susbsitem e alcatuit din mai multe statii de baza si RNC (Radio Network Controller).
    * Statia de baza comunica cu abonatul mobil printr-o interfata WCDMA (Wide CDMA)
    * MS e formata din echipamentul mobil, si SIM-ul, numit de acum __USIM__ _(UMTS Subscriber Identification Module)_.


* Functiile UTRAN:
    * Control asupra intregului sistem
    * Criptare/decriptare
    * Mobilitate
    * Controlul si mgmt-ul canalelor radio
    * Serviciile de difuzare


* Notam cu Um interfata dintre terminalul mobil si retea.

#### Arhitectura protocoalelor in UTMS
![pic4](C12_04.jpeg)
* Notatii:
    * PDCP = packet Data Convergence Protocolul
    * BMC = Broadcast/Multicast Control
    * NAS = Non-Access Stratum
    * RRM = Radio Resource Manacement
    * RRC = Radio Resource Control


* Nivelul fizic ofera canale de transport pentru nivelul MAC. Pe nivelul LD avem protocoalele MAC si LLC, iar in planul utilizator avem protocolul PDCP si BMC-ul.
* La nivel de retea, exista NAS. Protocolul NAS este intre _core network_ si echipamentul utilizatorului si asigura toate semnalizarile necesare pentru mentinerea conectivitatii IP intre echipamentul utilizatorului (UE) si un gateway catre o retea externa.


* RRC are functiile
    * RRM (Radio Resource Management)
    * Managementul mobilitatii (MM)
    * Managementul conexiunilor (CM)
    * Controlul legaturii logice (LLC)

* In modul _FDD_ (duplex in frecventa), UMTS are :
    * banda de uplink 1920-1980MHz    
    * banda de downlink ??

* Pentru duplex temproal:
    * Uplink 1900-1920
    * Downlink 2020-2025MHz


* Imprastierea spectrala se face cu o rata de 3.84 MChips.
    * [Chip](https://en.wikipedia.org/wiki/Chip_(CDMA) = secventa de +/-1 multiplicat cu o secventa de date.
    * Rata de cip a unui cod: numarul de impulsuri pe secunda cu care codul e transmis / receptionat. Un simbol e reprezentat de mai multi chips.
    * Factorul de imprastiere: rata de chips / rata de simbol.


* Distanta dintre purtatoare: 4.2-5MHz, iar pentru fiecare purtatoare avem un cadru format din 15 sloturi.
    * 1 cadru: 10ms
        * 1 cadru = 15 sloturi de 0.667ms= 2560chips

* Canalele de transport sunt expandate (puse intr-o banda mai larga) cu ajutorul [codurilor de spreading](https://en.wikipedia.org/wiki/Spread_spectrum) si sunt marcate cu coduri de scrambling, pentru identificarea MS sau  BS.   

### Imprastierea spectrala folosind tehnologia WCDMA:   
![last](C14_last.jpeg)

---
## [08.05.19] Curs Z
### __ [ ! ] Examen Ora 11 A01 - cu calculatoare (necesare neaparat) __
### Generatia 3.5G
* HSDPA (High Speed Downlink Packet Access) e bazata pe o arhitectura distribuita care permite o reactie la calitatea canalului si realizeaza si adaptarea conexiunii la intarzieri (mai ales la cele reduse)
* Multe din procesele de control si programare sunt preluat de la RNC si transferate la BS.


* HSDPA itroduce, cum zice numele, un canal descendent de mare viteza, notat HSCDSCH (High Speed Downlink Shared Channel). E folosit partajat de mai multi utilizatori, alocarea e dinamica intre utilizatori, iar viteza pe acest canal e de 42Mbps.
* In HSPDA se realizeaza o programare mai eficienta a pachatelor de transmisie de date pe baza informatiilor care sunt obtinute de la terminal despre:
    * calitatea canalului
    * capacitatea terminalului
    * info despre disponibilul de resurse la interfata radio (canale).
    * info despre necesitatile realizarii QoS
* Daca din cauza interferentelor / alte cauze, nu se poate face o decodare a pachetului, atunci e necesara retransmiterea lui
* Decizia de retransmitere se ia la nivelul BS, nu la RNC.


* Exista un canal fizic dedicat pentru control, notat __HSDPCCH__ (High Speed Dedicated Physical Control Channel).
* La nivelul BS, se colecteaza info despre calitatea canalelor obtinute de la fiecare abonat mobil si se stabileste in ce moment fiecare abonat poate sa foloseasca acest canal de control rapid.
* HSPDA poate folosi transmisia duplex si in timp si in frecventa, si in general HSDPA nu se foloseste pentru serviciile de timp real.


* __HSUPA__ (High Speed Uplink Packet Access) ofera viteze mai mari (5.76 Mbps) și se reduce semnificativ întârzierea pentru canalele dedicate.
* Se aduc avantaje transmisiilor de trafic de tip flux (videoclipuri, multimedia, email). Se foloseste 16QAM ca modulatie.
* Se acorda prioritati utilizatorilor


#### __IMS (IP Multimedia Subsystem)__
* folosit pe scara larga in LTE (traficul de voce nu e posibil in LTE fara el).
* IMS e o arhitectura complet noua care are la baza standardul VoIP. A fost creat pentru a oferi operatorilor un standard pt telefonia internet.
* Avantaj IMS: nu depinde de tehnologia rețelei.

Structura unui IMS:
![1](C_ultim_1.jpeg)
* SIP -> protocol de control
* se folseste IPv6


* IMS are principalele caracteristici:
    * ?
    * independent de tehnologie
    * functional separat de reteaua de baza cu comutare de pachete
    * posibilitatea de a realiza comunicatii multimedia de la persoana la persoana in timp real
    * capacitatea de a integra complet comunicatiile multimedia desfasurate in timp real cu cele in mod nonreal si comunicatiile om-masina.
    * faciliteaza capacitatea de interactiune intre diferite servicii si aplicatii
    * ofera abonatilor capacitatea de a avea acces la servicii multimedia intr-o singura sesiune, sau in sesiuni multiple, simultan sincronizate


### __Generatia LTE__
* LTE e "aproape" 4G (nu a atins inca standardele).
* Spre deosebire de generatiile anterioare, LTE nu mai e ierarhic, ci descentralizat.
* Contine o parte radio si o parte de control, nucelul de retea IP.
* Abonatul mobil se noteaza UE (User Equipment)
* Modulatia e 64QAM pentru downlink si 16QAM pt uplink, se completeaza cu MIMO 4x4.

Schema bloc a unei retele LTE:
![2](C_ultim_2.png)
* Nucleul de retea IP serveste tot mai putin SMS-urile.
* Sunt incluse mai putine elemente de arhitectura logice si fizice decat in UMTS
* Dispozitivele LTE trebuie sa fie capabile sa suporte tehnologiile care deja exista (GSM, GPRS, UMTS).
* Protocoalele si interfetele sunt concepute intrucat trecerea de la o tehnologie la alta sa se faca fara timp de asteptare.
    * E.g.: LTE->UMTS->GSM fara probleme.
    * GSM->UMTS->LTE ---> HOPA


* eNB  = cel mai complex dispozitiv din LTE, continand 3 elemente de baza:
    * antena
    * modulele radio care mod/demod semnalele
    * modulele pentru procesarea semnalelor primite si receptionate


* Functii eNB:
    * management abonati si planificare resurse
    * asigurarea QoS
    * asigurarea unui echilibru intre diferite servicii radio simultane
    * managementul mobilitatii
    * managementul interferentelor


* Interfete importante din schema:
    * __Uu__: interfata dintre UE si eNB - singura interfata din sistem care este intotdeauna wireless
        * viteze de pana la 150Mbps (poate fi si mai mica din cauza interferentei, distantei de la antena la abonat, sau depinde de incarcarea celulei).
    * __S1__: intre eNB si nucleul retelei
        * 2 componente logice: S1_UserPlane și S1_ControlPlane
        * S1_CP are 2 functii:
            * statia de baza eNB sa interactioneze cu nucleul retelei pt comunicatii specifice
            * transferul semnalizarilor care tin de abonati (e.g. pt transmisii vocale)
    * __X2__: intre eNB-uri
        * asigura si controleaza _handover_-ul
        * functii legate de coordonarea interferentelor


* __MME__ (Mobility Management Entity)
    * Asigura toate schimburile de semnalizari dintre statiile de baza si nucleul retelei.
    * Functii:
        * autentificare (verificarea inregistrarii abonatului in HSS)
        * stabilirea unui tunel IP intre BS si gateway-ul catre Internet.
        * urmarirea dispozivelor abonatilor inactivi. Daca e necesara o tx de date catre un astfel de abonat, pozitia in retea va fi deja cunoscuta.
        * asigura un suport de handover
        * comunicarea cu alte retele: daca acoperirea LTE nu mai e disponibila, MME decide daca abonatul trece intr-o retea UMTS, sau GSM, sau daca trece in alta celula.
        * MME se ocupa  cu transmisiile de voce si SMS


* __SGW__ (Serving Gateway)
    * asigura tunelele datelor abonatilor intre eNB-uri si PDN-GW.


* __PDN-GW__ (Packet Data Network Gateway)
    * primeste pachetele de la abonatii din retelele externe, le incapsuleaza in tunele si le trimite catre SGW.
    * PDN-GW aloca adrese IP abonatilor mobili.
    * [?] blank
    * in LTE se pot aloca mai multe adrese IP (IPv4/6) unui singur abonat.
    * rol important in _Roaming_: cand abonatul se deplaseaza, se creeaza legatura intre PDN-GW din reteaua initiala si SGW din reteaua noua in care abonatul se afla in vizita.


* __HSS__ (Home Subscriber Server) - inlocuieste HLR
    * pentru comunicarea intre HSS si celelalte module se foloseste protocolul numit *__diameter__*.
    * In HSS sunt continute:
        * IMSI
        * cheile de criptare
        * informatii specifice IMS-ului (ID MSC folosit, ID SGSN sau MME)


#### __Stiva de protocoale pentru interfata S1__
* SCTP (Stream Control Transmission Protocol)
* NAS (Non-Access Protocol)
    * asigura suportul pt mobilitatea UE și mentinerea conectivitatii IP intre UE si PDNGW
* RRC (Radio Res. Control):
    * asigura paging
    * mentinerea si utilizarea conexiunii intre utilizator si interfata radio
    * functii de securitate (managementul cheilor de criptare)
    asigura stabilirea, configurarea, mentinerea si eliberarea de purtatori radio
    * asigura functii legate de mobilitate, de mgmt al QoS-ului
    * raporteaza masuratorile de semnal de la abonat și face controlul (incadrarea intre limite)
* PDCP (Packet Data Convergence Protocol)
    * compresia/decompresia header-elor
    * asigura criptarea si decriptarea și transferul de date al utilizatorilor.
 RLC (Radio Link Control)
    * asigura corectia operatorilor, detectia duplicatelor și detecția de erori de protocol.

![3](Protocol+Stack+Functionality.jpg)
