# __ETTI\_RST\_AN4\_SERIAD\_2018/19__

__Knowledge should be open source.__  
Cine vrea sa invete, sa aibe de unde.

*__P.S.__ Daca sunt oameni care doresc sa extinda repo-ul cu alte materiale (linkuri de drive/dropbox, etc.), [contactati-ma](http://www.facebook.com/theiosif) (sau pe telegram `@theiosif`)*.

## Cuprins cursuri _(work in progress)_
* [Retele de Comunicatii Mobile](RCM/RCM.md)
* [Tehnici si Sisteme de Transmisii Multiplex _(sheet Materiale)_](https://goo.gl/DFKEi2)
* [Calitate si Fiabilitate](http://www.euroqual.pub.ro/cursuri/calitate-fiabilitate/)
* [RS, PDF din 10.05.19](RS.pdf)
